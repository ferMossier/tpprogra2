package CentroMedico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class Centro {

	// VARIABLES DE INSTANCIA:

	private String cuit;
	private String nombreCentro;
	private double costoInternacionDiario;
	private LinkedList<Especialidad> listaEspecialidades = new LinkedList<Especialidad>();
	private HashMap<Integer, Paciente> listaPacientes = new HashMap<Integer, Paciente>();
	private HashMap<Integer, Medico> listaMedicos = new HashMap<Integer, Medico>();

	// CONSTRUCTOR:

	Centro(String nombreCentro, String cuit, float costoInternacionDiario) {
		this.cuit = cuit;
		this.nombreCentro = nombreCentro;
		if (costoInternacionDiario >= 0) {
			this.costoInternacionDiario = costoInternacionDiario;
		} else {
			throw new RuntimeException("El costo de internacion debe ser un valor positivo");
		}
	}

	// ESPECIALIDADES:

	public void agregarEspecialidad(String nombreEspecialidad, double costoEspecialidad) {
		if (!existeEspecialidad(nombreEspecialidad)) {
			if (costoEspecialidad >= 0) {
				listaEspecialidades.addLast(new Especialidad(nombreEspecialidad, costoEspecialidad));
			} else {
				throw new RuntimeException(
						"El costo de la especialidad " + nombreEspecialidad + " no puede ser negativo");
			}
		} else {
			throw new RuntimeException(
					"LA ESPECIALIDAD " + nombreEspecialidad + " QUE ESTA INTENTANDO AGREGAR, YA EXISTE");
		}
	}

	private boolean existeEspecialidad(String especialidad) { // Este metodo verifica si la especialidad ya existe.
		boolean estaEnLista = false;
		Iterator<Especialidad> it = listaEspecialidades.iterator();
		if (!listaEspecialidades.isEmpty()) {
			while (it.hasNext()) {
				if (it.next().consultarNombreEspecialidad().equalsIgnoreCase(especialidad)) {
					estaEnLista = estaEnLista || true;
				}
			}
		}
		return estaEnLista;
	}

	// MEDICO:

	public void agregarMedico(String nombreMedico, Integer matriculaMedico, String especialidad, double honorarios) {
		if (!existeMedico(matriculaMedico)) {
			Especialidad especialidad1 = obtenerEspecialidad(especialidad);
			if (especialidad1 != null) {
				agregarMedico(nombreMedico, matriculaMedico, especialidad1, honorarios);
			} 
			else {
				throw new RuntimeException("La especialidad del m�dico " + matriculaMedico + " no existe registrada en el centro");
			}
		} 
		else {
			throw new RuntimeException("LA MATRICULA " + matriculaMedico + " DEL MEDICO " + nombreMedico
					+ " QUE ESTA INTENTANDO AGREGAR, YA EXISTE ASOCIADA A "
					+ buscarMedico(matriculaMedico).consultarNombreMedico());
		}
	}

	public void agregarMedico(String nombreMedico, Integer matriculaMedico, Especialidad especialidad, double honorarios) {
		listaMedicos.put(matriculaMedico, new Medico(nombreMedico, matriculaMedico, especialidad, honorarios));
	}

	private boolean existeMedico(Integer matricula) { //Verifica si el medico existe en la base
		return (listaMedicos.containsKey(matricula)) ? true : false;
	}

	private Especialidad obtenerEspecialidad(String especialidad) { //Transforma el String especialidad y retorna el objeto especialidad que le corresponde.
		Especialidad especialidad1 = null;
		for (Especialidad especialidades : listaEspecialidades) {
			if (especialidades.consultarNombreEspecialidad().equalsIgnoreCase(especialidad)) {
				especialidad1 = especialidades;
			}
		}
		return especialidad1;
	}

	public Medico buscarMedico(Integer matriculaMedico) {
		if (listaMedicos.containsKey(matriculaMedico)) {
			return listaMedicos.get(matriculaMedico);
		} 
		else {
			throw new RuntimeException(
					"LA MATRICULA " + matriculaMedico + " NO SE ENCUENTRA REGISTRADA EN NUESTRO CENTRO");
		}
	}

	// PACIENTES:

	public void agregarPacientePrivado(String nombrePaciente, Integer historiaClinica, Fecha fechaNacimiento) {
		if (!existeHC(historiaClinica)) {
			listaPacientes.put(historiaClinica, new PacientePrivado(historiaClinica, nombrePaciente, fechaNacimiento));
		} 
		else {
			throw new RuntimeException("LA HC " + historiaClinica + " QUE ESTA INTENTANDO AGREGAR, YA EXISTE EN EL SISTEMA");
		}
	}

	public void agregarPacienteObraSocial(String nombrePaciente, Integer historiaClinica, Fecha fechaNacimiento, String obraSocial, double porcentajeAPagar) {
		if (!existeHC(historiaClinica)) {
			listaPacientes.put(historiaClinica, new PacienteObraSocial(historiaClinica, nombrePaciente, fechaNacimiento, obraSocial, porcentajeAPagar));
		} 
		else {
			throw new RuntimeException(
					"LA HC " + historiaClinica + " QUE ESTA INTENTANDO AGREGAR, YA EXISTE EN EL SISTEMA");
		}
	}

	public void agregarPacienteAmbulatorio(String nombrePaciente, Integer historiaClinica, Fecha fechaNacimiento) {
		if (!existeHC(historiaClinica)) {
			listaPacientes.put(historiaClinica,	new PacienteAmbulatorio(historiaClinica, nombrePaciente, fechaNacimiento));
		} 
		else {
			throw new RuntimeException("LA HC " + historiaClinica + " QUE ESTA INTENTANDO AGREGAR, YA EXISTE EN EL SISTEMA");
		}
	}

	private boolean existeHC(Integer historiaClinica) {
		if (listaPacientes.containsKey(historiaClinica)) {
			return true;
		}
		return false;
	}

	public Paciente buscarPaciente(Integer historiaClinica) {
		if (!existeHC(historiaClinica)) {
			throw new RuntimeException("EL PACIENTE QUE BUSCA NO EXISTE EN NUESTRO SISTEMA");
		} else {
			return listaPacientes.get(historiaClinica);
		}
	}

	public double getSaldo(Integer historiaClinica) {
		if(existeHC(historiaClinica)) {
			return  buscarPaciente(historiaClinica).consultarDeuda();
		}else {
			throw new RuntimeException("No se puede acceder al saldo porque la historia clinica no se encuentra registrada");
		}
	}

	public void pagarSaldo(Integer historiaClinica) {
		if(existeHC(historiaClinica)) {
			buscarPaciente(historiaClinica).saldarDeuda();
			asentarPago(historiaClinica);
		}else {
			throw new RuntimeException("No se puede pagar el saldo porque la historia clinica no se encuentra registrada");
		}	
	}

	// Este metodo se encarga de dejar asentado en el registro de atencion del
	// paciente que corresponda, si pago la atencion.
	// Por defecto, en todos los registros queda asentado que no pago la atencion
	// (false) y cuando el cliente paga el saldo
	// se llama al metodo asentarPago para dejar registrado. Con este metodo se
	// cumple lo solicitado en las observaciones,
	// punto 4 de la segunda parte del tp.

	public void asentarPago(Integer historiaClinica) {
		if (buscarPaciente(historiaClinica) instanceof PacientePrivado) {
			RegistroPrivado reg = ((PacientePrivado) buscarPaciente(historiaClinica)).obtenerUltimoRegistro();
			reg.asentarPago();
		} else if (buscarPaciente(historiaClinica) instanceof PacienteAmbulatorio) {
			RegistroTratamiento reg = ((PacienteAmbulatorio) buscarPaciente(historiaClinica)).obtenerUltimoRegistro();
			reg.asentarPago();
		} else if (buscarPaciente(historiaClinica) instanceof PacienteObraSocial
				&& !((PacienteObraSocial) buscarPaciente(historiaClinica)).estaInternado()) {
			RegistroInternacion reg = ((PacienteObraSocial) buscarPaciente(historiaClinica)).obtenerUltimoRegistro();
			reg.asentarPago();
		} else if (buscarPaciente(historiaClinica) instanceof PacienteObraSocial
				&& ((PacienteObraSocial) buscarPaciente(historiaClinica)).estaInternado()) {
			RegistroInternacion reg = ((PacienteObraSocial) buscarPaciente(historiaClinica)).obtenerListaInternacion()
					.get(((PacienteObraSocial) buscarPaciente(historiaClinica)).obtenerListaInternacion().size() - 2);
			reg.asentarPago();
		}
	}

	// ATENCIONES/INTERNACIONES/TRATAMIENTOS:

	private boolean tuvoAtencionEnFecha(Integer hC, Fecha fecha) {
		boolean coincideFecha = false;
		if (buscarPaciente(hC) instanceof PacientePrivado) {
			PacientePrivado pV = (PacientePrivado) buscarPaciente(hC);
			if (pV.tuvoAtenciones()) {
				for (RegistroPrivado reg : pV.obtenerRegistroAtenciones()) {
					if (reg.consultarFechaIngreso().equals(fecha)) {
						coincideFecha = coincideFecha || true;
					}
				}
			}
		}
		return coincideFecha;
	}
	
	public void agregarAtencion(int hC, Fecha fecha, int matricula) { // REGISTRO ATENCION CONSULTORIO (PP)
		registrarAtencionConsultorio(hC, fecha, matricula);
	}

	public void agregarAtencion(int hC, Fecha fecha) { // REGISTRO ATENCION GUARDIA (PP)
		registrarAtencionGuardia(hC, fecha);
	}

	public void registrarAtencionConsultorio(Integer historiaClinica, Fecha fecha, Integer matriculaMedico) {
		if (!tuvoAtencionEnFecha(historiaClinica, fecha)) {
			Medico medico1 = listaMedicos.get(matriculaMedico);
			double tarifaEspecialidad = medico1.obtenerEspecialidad().consultarTarifa();
			if (buscarPaciente(historiaClinica) instanceof PacientePrivado) {
				PacientePrivado pPriv = (PacientePrivado) buscarPaciente(historiaClinica);
				pPriv.agregarRegistroConsultorio(fecha, medico1);
				pPriv.agregarDeuda(tarifaEspecialidad);
			} else {
				throw new RuntimeException(
						"El paciente " + historiaClinica + " no puede acceder a una atencion por consultorio");
			}
		}
	}

	public void registrarAtencionGuardia(int historiaClinica, Fecha fecha) {
		if (!tuvoAtencionEnFecha(historiaClinica, fecha)) {
			if (buscarPaciente(historiaClinica) instanceof PacientePrivado) {
				((PacientePrivado) buscarPaciente(historiaClinica)).agregarRegistroGuardia(fecha);
			} else {
				throw new RuntimeException("El paciente " + historiaClinica + " no puede acceder a una guardia");
			}
		}
	}

	public void agregarInternacion(int historiaClinica, String area, Fecha fecha) {
		registrarInternacion(historiaClinica, fecha, area);
	}

	public void registrarInternacion(int historiaClinica, Fecha fechaIngreso, String area) {
		if (area.equalsIgnoreCase("general") || area.equalsIgnoreCase("pediatria")
				|| area.equalsIgnoreCase("cardiologia")) {
			if (buscarPaciente(historiaClinica) instanceof PacienteObraSocial) {
				PacienteObraSocial pOS = (PacienteObraSocial) buscarPaciente(historiaClinica);
				if (!pOS.obtenerListaInternacion().isEmpty()) {
					if (!pOS.estaInternado()
							&& !pOS.obtenerUltimoRegistro().consultarFechaIngreso().equals(fechaIngreso)) {
						pOS.agregarRegistroInternacion(fechaIngreso, area);
					} else {
						throw new RuntimeException("El paciente " + historiaClinica
								+ " se encuentra internado actualmente o ya fue internado y dado de alta el d�a de hoy");
					}
				} else {
					pOS.agregarRegistroInternacion(fechaIngreso, area);
				}
			} else {
				throw new RuntimeException("El paciente que est� intentado internar no es de Obra Social");
			}
		} else {
			throw new RuntimeException("EL AREA DE INTERNACION " + area + " NO EXISTE");
		}
	}

	public void altaInternacion(int historiaClinica, Fecha fechaEgreso) {
		double costoInternacionfinal = 0;
		if (buscarPaciente(historiaClinica) instanceof PacienteObraSocial) {
			PacienteObraSocial pOS = (PacienteObraSocial) buscarPaciente(historiaClinica);
			if (pOS.estaInternado() && (pOS.obtenerUltimoRegistro().consultarFechaIngreso().esMenor(fechaEgreso) || pOS.obtenerUltimoRegistro().consultarFechaIngreso().equals(fechaEgreso))) {
				pOS.obtenerUltimoRegistro().ingresarFechaEgreso(fechaEgreso);
				pOS.setInternado(false);
				costoInternacionfinal = Fecha.diferenciaDeDias(pOS.obtenerUltimoRegistro().consultarFechaIngreso(),
						fechaEgreso) * costoInternacionDiario * pOS.consultarPorcentajeAPagar();
				pOS.agregarDeuda(costoInternacionfinal);
			} else {
				throw new RuntimeException("LA FECHA DE ALTA ES MENOR A LA FECHA DE INTERNACION");
			}
		} else {
			throw new RuntimeException(
					"El paciente " + historiaClinica + " que est� intentado dar de alta no es de Obra Social");
		}
	}

	public void agregarTratamiento(int historiaClinica, int matriculaMedico, String nombreTratamiento) {
		registrarAtencionTratamiento(historiaClinica, matriculaMedico, nombreTratamiento);
	}

	public void registrarAtencionTratamiento(int historiaClinica, int matriculaMedico, String tratamiento) {
		Medico medico1 = listaMedicos.get(matriculaMedico);
		double costoTratamiento = medico1.consultarHonorarios();
		if (buscarPaciente(historiaClinica) instanceof PacienteAmbulatorio) {
			PacienteAmbulatorio pAmbulatorio = (PacienteAmbulatorio) buscarPaciente(historiaClinica);
			pAmbulatorio.agregarRegistroTratamiento(medico1, tratamiento);
			pAmbulatorio.agregarDeuda(costoTratamiento);
		} else {
			throw new RuntimeException("El paciente " + historiaClinica + " no puede acceder a un tratamiento");
		}
	}
	
	public ArrayList<Integer> listaInternacion() {
		ArrayList<Integer> listaPacientesInternados = new ArrayList<Integer>();
		for (Paciente paciente : listaPacientes.values()) {
			if (paciente instanceof PacienteObraSocial) {
				if (((PacienteObraSocial) paciente).estaInternado()) {
					listaPacientesInternados.add(paciente.historiaClinica);
				}
			}
		}
		return listaPacientesInternados;
	}

	public HashMap<Fecha, String> atencionesEnConsultorio(int hC) {
		return ((PacientePrivado) buscarPaciente(hC)).atencionesEnConsultorio(hC);
	}

	// De uso exclusivo para el toString
	private String Especialidades() {
		StringBuilder especialidades = new StringBuilder();
		for (Especialidad esp : listaEspecialidades) {
			especialidades.append(esp.consultarNombreEspecialidad());
			especialidades.append(", ");
		}
		return especialidades.toString();
	}

	@Override
	public String toString() {
		int cantidadPacientes = listaPacientes.size();
		int cantidadMedicos = listaMedicos.size();

		StringBuilder centro = new StringBuilder();
		centro.append("Nombre: ").append(nombreCentro);
		centro.append("\nCuit: ").append(cuit);
		centro.append("\nTiene un total de ").append(cantidadPacientes).append(" pacientes");
		centro.append("\nTiene un total de ").append(cantidadMedicos).append(" medicos");
		centro.append("\nSus especialidades son: ").append(this.Especialidades());
		centro.append("\nSu costo diario de internacion es: $").append(this.costoInternacionDiario);

		return centro.toString();
	}
}