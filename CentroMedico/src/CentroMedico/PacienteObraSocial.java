package CentroMedico;

import java.util.ArrayList;

public class PacienteObraSocial extends Paciente {
	private ArrayList<RegistroInternacion> registroDeInternacion;
	private String obraSocial;
	private double porcentajeAPagar;
	private boolean internado;

	public PacienteObraSocial(int historiaClinica, String nombrePaciente, Fecha fechaNacimiento, String obraSocial, double porcentajeAPagar) {
		super(nombrePaciente, historiaClinica, fechaNacimiento); // Al crear un paciente nuevo, su deuda inicial es 0
		this.obraSocial = obraSocial;
		this.registroDeInternacion = new ArrayList<RegistroInternacion>();
		this.internado = false;
		if (porcentajeAPagar >= 0 && porcentajeAPagar <= 1) {
			this.porcentajeAPagar = porcentajeAPagar;	
		} else {
			throw new RuntimeException("Ingrese un porcentaje valido, entre 0 y 1");
		}
	}

	public void agregarRegistroInternacion(Fecha fechaIngreso, String area) {
		registroDeInternacion.add(new RegistroInternacion(fechaIngreso, area));
		this.internado = true;
	}

	public ArrayList<RegistroInternacion> obtenerListaInternacion() {
		return registroDeInternacion;
	}

	public RegistroInternacion obtenerUltimoRegistro() {
		if (!registroDeInternacion.isEmpty()) {
			return registroDeInternacion.get(registroDeInternacion.size() - 1);
		} else {
			throw new RuntimeException("El registro esta vacio");
		}
	}

	public boolean estaInternado() {
		return this.internado;
	}

	public void setInternado(boolean i) {
		this.internado = i;
	}

	public double consultarPorcentajeAPagar() {
		return porcentajeAPagar;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(", Obra Social: ").append(obraSocial);
		return sb.toString();
	}
}
