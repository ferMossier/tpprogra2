package CentroMedico;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class TestsJUnit {
	Centro centro;
	
	@Before
	public void setUp() throws Exception {
		centro = new Centro("Clinica Dolores Fuertes", "20-11111111-2", 1500);
		
		centro.agregarEspecialidad("Gastroenterologia", 6000);
		centro.agregarEspecialidad("Neurologia", 1500);
		centro.agregarEspecialidad("Oftalmologia", 2500);
		centro.agregarEspecialidad("Pediatria", 600);
		centro.agregarEspecialidad("Neumonologia", 2000);
		centro.agregarEspecialidad("Oncologia", 12000);
		centro.agregarEspecialidad("Urologia", 0);
		
		centro.agregarMedico("Pepe", 777, "Urologia", 0);
		centro.agregarMedico("Carlos", 1234, "Pediatria", 8000);
		centro.agregarMedico("Ana", 6969, "Neurologia", 6000);
		
		centro.agregarPacientePrivado("Armando Esteban Quito", 555, new Fecha(2,12,1990));
		centro.agregarPacienteAmbulatorio("Aquiles Castro", 666, new Fecha(8,11,1995));
		centro.agregarPacienteObraSocial("Aldo Lorido", 234, new Fecha(25,12,1960), "TregarOS", 0.5);

		centro.agregarAtencion(555, new Fecha(31,12,2019));
		centro.agregarAtencion(555, new Fecha(1,1,2020));

		centro.agregarAtencion(555, new Fecha(1,1,2020),777);
		centro.agregarAtencion(555, new Fecha(2,1,2020),777);
		centro.agregarAtencion(555, new Fecha(3,1,2020),777);

		centro.agregarInternacion(234, "General", new Fecha(20,11,2020));
		centro.altaInternacion(234, new Fecha(28,11,2020));
		centro.agregarInternacion(234, "Cardiologia", Fecha.hoy());
		centro.altaInternacion(234, Fecha.hoy());
		
		centro.agregarTratamiento(666, 6969, "Quimioterapia");
		centro.agregarTratamiento(666, 1234, "Terapia Hormonal");
	
	}

	@Test
	public void testSaldoPacientePrivado() {
		assertEquals(0,centro.getSaldo(555),10);
		centro.pagarSaldo(555);
		assertEquals(0,centro.getSaldo(555),1);
	}

	@Test
	public void testSaldoPacienteOSocial() {
		assertEquals(6000,centro.getSaldo(234),20);
		centro.pagarSaldo(234);
		assertEquals(0,centro.getSaldo(234),1);
	}

	@Test
	public void testSaldoPacienteAmbulatorio() {
		assertEquals(14000,centro.getSaldo(666),10);
		centro.pagarSaldo(666);
		assertEquals(0,centro.getSaldo(666),1);
	}

	@Test
	public void testAtencionesEnConsultorio() {
		Map<Fecha,String> aten = centro.atencionesEnConsultorio(555);
		assertTrue(aten.values().contains("Urologia"));
	}

	@Test
	public void testListaInternacion() {
		centro.agregarInternacion(234, "Cardiologia", new Fecha(10,10,2020));
		centro.agregarPacienteObraSocial("Lola Mento", 999, new Fecha(6,8,2000),"Canape", 0.4);
		centro.agregarInternacion(999, "Pediatria", new Fecha(12,11,2020));
		centro.altaInternacion(234, Fecha.hoy());
		centro.agregarPacienteObraSocial("Susana Oria", 765, new Fecha(9,7,1989),"Osde", 0.1);
		centro.agregarInternacion(765, "Cardiologia", new Fecha(21,11,2020));
		List<Integer> internados = centro.listaInternacion();
		assertFalse(internados.contains(432));
		assertTrue(internados.contains(765));
		assertFalse(internados.contains(654));
	}
}
