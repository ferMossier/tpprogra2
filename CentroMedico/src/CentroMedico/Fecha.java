package CentroMedico;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Fecha {
	private int dia;
	private int mes;
	private int a�o;

	public Fecha(int dia, int mes, int a�o) {
		LocalDate fecha = LocalDate.of(a�o, mes, dia);

		this.dia = fecha.getDayOfMonth();
		this.mes = fecha.getMonthValue();
		if (a�o > 1800 && a�o <= LocalDate.now().getYear())
			this.a�o = fecha.getYear();
		else
			throw new RuntimeException("INGRESE UN A�O VALIDO");
	}

	private static LocalDate fechaToLD(Fecha fecha) {
		return LocalDate.of(fecha.a�o, fecha.mes, fecha.dia);
	}

	public static int diferenciaDeDias(Fecha fechaIngreso, Fecha fechaEgreso) {
		return (int) ChronoUnit.DAYS.between(fechaToLD(fechaIngreso), fechaToLD(fechaEgreso));
	}

	public static Fecha hoy() {
		LocalDate fecha = LocalDate.now();
		return new Fecha(fecha.getDayOfMonth(), fecha.getMonthValue(), fecha.getYear());
	}

	public int a�os() {
		return (int) ChronoUnit.YEARS.between(fechaToLD(this), LocalDate.now());
	}

	public boolean esMenor(Fecha fecha) {
		LocalDate fecha1 = fechaToLD(fecha);
		LocalDate fechaThis = fechaToLD(this);
		if (fecha1.isAfter(fechaThis)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean esMayor(Fecha fecha) {
		LocalDate fecha1 = fechaToLD(fecha);
		LocalDate fechaThis = fechaToLD(this);
		if (fecha1.isBefore(fechaThis)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Fecha: ").append(dia).append("/").append(mes).append("/").append(a�o);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Fecha otro = (Fecha) obj;
		if (a�o != otro.a�o)
			return false;
		if (dia != otro.dia)
			return false;
		if (mes != otro.mes)
			return false;
		return true;
	}

}