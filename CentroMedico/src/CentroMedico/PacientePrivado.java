package CentroMedico;
//import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class PacientePrivado extends Paciente {
	private ArrayList<RegistroPrivado> registroDeAtencion;
	
	PacientePrivado(int historiaClinica, String nombrePaciente, Fecha fechaNacimiento){
		super(nombrePaciente, historiaClinica, fechaNacimiento);//Al crear un paciente nuevo, su deuda inicial es 0
		this.registroDeAtencion = new ArrayList<RegistroPrivado>();
	}	
	
	public HashMap<Fecha, String> atencionesEnConsultorio(int hC) {
		HashMap<Fecha, String> listaDeAtencionesConsultorio= new HashMap<Fecha, String>();
		for (RegistroPrivado registro : registroDeAtencion) {
			if (registro.atencionGuardia()==false) {
			listaDeAtencionesConsultorio.put(registro.consultarFechaIngreso(), registro.consultarEspecialidadDeAtencion().consultarNombreEspecialidad());
			}
		}
		return listaDeAtencionesConsultorio;
	}
	
	public void agregarRegistroConsultorio(Fecha fechaIngreso, Medico medico) {
		registroDeAtencion.add(new RegistroPrivado(fechaIngreso, medico));
	}

	public void agregarRegistroGuardia(Fecha fechaIngreso) {
		registroDeAtencion.add(new RegistroPrivado(fechaIngreso));
	}
	
	public RegistroPrivado obtenerUltimoRegistro() {
		if(!registroDeAtencion.isEmpty()) {
			return registroDeAtencion.get(registroDeAtencion.size()-1);	
		}else {
			throw new RuntimeException("El registro esta vacio");
		}	
	}
	
	public boolean tuvoAtenciones() {
		if(registroDeAtencion.size()>0) {
			return true;
		}
		return false;
	}
	
	public ArrayList<RegistroPrivado> obtenerRegistroAtenciones() {
		return registroDeAtencion;
	}
}