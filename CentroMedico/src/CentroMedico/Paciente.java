package CentroMedico;

//Edad mayor o igual a cero

public abstract class Paciente {

	protected String nombrePaciente;
	protected int historiaClinica;
	protected Fecha fechaNacimiento;
	protected double deuda;

	public Paciente(String nombrePaciente, int historiaClinica, Fecha fechaNacimiento) {
		this.nombrePaciente = nombrePaciente;
		this.historiaClinica = historiaClinica;
		this.deuda = 0;
		if (fechaNacimiento.esMenor(Fecha.hoy())) {
			this.fechaNacimiento = fechaNacimiento;
		} else {
			throw new RuntimeException("Ingrese una fecha de nacimiento valida para el paciente " + historiaClinica);
		}
	}

	void agregarDeuda(double deuda) {
		this.deuda = this.deuda + deuda;
	}

	double consultarDeuda() {
		return deuda;
	}

	void saldarDeuda() {
		this.deuda = 0;
	}

	int obtenerHistoriaClinica() {
		return historiaClinica;
	}

	String obtenerNombrePaciente() {
		return nombrePaciente;
	}

	Fecha obtenerFechaNacimiento() {
		return fechaNacimiento;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Paciente nombre: ").append(nombrePaciente);
		sb.append(", Historia Clinica: ").append(historiaClinica);
		sb.append(", Edad: ").append(fechaNacimiento.a�os()).append(" a�os");
		return sb.toString();
	}

}
