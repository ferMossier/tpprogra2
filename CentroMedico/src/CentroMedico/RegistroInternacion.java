package CentroMedico;

public class RegistroInternacion {

	private String area;
	private Fecha fechaIngreso;
	private Fecha fechaEgreso;
	private boolean pagoAtencion;

	// Constructor
	public RegistroInternacion(Fecha fechaIngreso, String area) {
		this.fechaIngreso = fechaIngreso;
		this.fechaEgreso = null;
		this.area = area;
		this.pagoAtencion = false;
	}

	public void ingresarFechaEgreso(Fecha fechaEgreso) {
		this.fechaEgreso = fechaEgreso;
	}

	public String consultarAreaDeInternacion() {
		return this.area;
	}

	public Fecha consultarFechaIngreso() {
		return this.fechaIngreso;
	}

	public Fecha consultarFechaEgreso() {
		return this.fechaEgreso;
	}

	public void asentarPago() {
		this.pagoAtencion = true;
	}

	public boolean devolverEstadoPago() {
		return this.pagoAtencion;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[fechaIngreso: ").append(fechaIngreso);
		sb.append(", fechaEgreso: ").append(fechaEgreso);
		sb.append("area: ").append(area);
		sb.append(", pagoAtencion: ").append(pagoAtencion).append("]");
		return sb.toString();
	}
}
