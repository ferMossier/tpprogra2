package CentroMedico;

public class Medico {

	private String nombreMedico;
	private int matricula;
	private Especialidad especialidad;
	private double honorarios;

	// Constructor

	Medico(String nombreMedico, int matricula, Especialidad especialidad, double honorarios) {
		this.nombreMedico = nombreMedico;
		this.matricula = matricula;
		this.especialidad = especialidad;
		if (honorarios >= 0) {
			this.honorarios = honorarios;
		} else {
			throw new RuntimeException("Los honorarios del m�dico " + nombreMedico + " no pueden ser negativos");
		}
	}

	public String consultarNombreMedico() {
		return nombreMedico;
	}

	public Especialidad obtenerEspecialidad() {
		return especialidad;
	}

	public double consultarHonorarios() {
		return honorarios;
	}

	public int obtenerMatricula() {
		return matricula;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(); 
		sb.append("[Nombre: ").append(nombreMedico);
		sb.append(", matricula: ").append(matricula);
		sb.append(", especialidad: ").append(especialidad);
		sb.append(", honorarios: ").append(honorarios).append("]");
		return sb.toString();
	}

}
