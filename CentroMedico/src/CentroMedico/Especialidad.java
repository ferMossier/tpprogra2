package CentroMedico;

//Costo mayor o igual a cero

public class Especialidad {

	private String nombreEspecialidad;
	private double costoEspecialidad;

	// Constructor

	Especialidad(String nombreEspecialidad, double costoEspecialidad) {
		this.nombreEspecialidad = nombreEspecialidad;
		this.costoEspecialidad = costoEspecialidad;
	}

	double consultarTarifa() {
		return costoEspecialidad;
	}

	String consultarNombreEspecialidad() {
		return nombreEspecialidad;
	}

	@Override
	public String toString() {
		return nombreEspecialidad;
	}

}
