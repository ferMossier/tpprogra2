package CentroMedico;

public class RegistroTratamiento {
	private String tratamiento;
	private Medico medico;
	private boolean pagoAtencion;

	//Constructor
	RegistroTratamiento(Medico medico, String tratamiento){
		this.medico=medico;
		this.tratamiento=tratamiento;
		this.pagoAtencion=false;
	}
	
	public Medico obtenerMedico() {
		return medico;
	}
	
	public String consultarTratamiento() {
		return  tratamiento;
	}
	
	public void asentarPago() {
		pagoAtencion=true;
	}
	
	public boolean devolverEstadoPago() {
		return pagoAtencion;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[tratamiento: ").append(tratamiento);
		sb.append(", medico: ").append(medico);
		sb.append(", pagoAtencion: ").append(pagoAtencion).append("]");
		return sb.toString();
	}
}
