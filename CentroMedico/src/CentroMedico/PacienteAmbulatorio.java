package CentroMedico;

//import java.time.LocalDate;
import java.util.ArrayList;

public class PacienteAmbulatorio extends Paciente {
	// Variables de instancia
	private ArrayList<RegistroTratamiento> registroDeAtencion;

	// Constructor
	public PacienteAmbulatorio(int historiaClinica, String nombrePaciente, Fecha fechaNacimiento) {
		super(nombrePaciente, historiaClinica, fechaNacimiento);// Al crear un paciente nuevo, su deuda inicial es 0
		this.registroDeAtencion = new ArrayList<RegistroTratamiento>();
	}

	public void agregarRegistroTratamiento(Medico medico, String tratamiento) {
		registroDeAtencion.add(new RegistroTratamiento(medico, tratamiento));
	}

	public RegistroTratamiento obtenerUltimoRegistro() {
		return registroDeAtencion.get(registroDeAtencion.size() - 1);
	}
	
	public ArrayList<RegistroTratamiento> obtenerRegistroTratamientos() {
		return registroDeAtencion;
	}
}
