package CentroMedico;
//import java.time.LocalDate;

public class RegistroPrivado {

	//Variables de instancia
	private Especialidad especialidad;
	private Medico medico;
	private Fecha fechaIngreso;
	private boolean esGuardia;
	private boolean pagoAtencion;

	// Constructor
	RegistroPrivado(Fecha fechaIngreso, Medico medico) { // Atencion consultorio
		this.fechaIngreso = fechaIngreso;
		this.medico = medico;
		this.especialidad = medico.obtenerEspecialidad();
		this.pagoAtencion = false;
	}

	RegistroPrivado(Fecha fechaIngreso) { // Atencion Guardia
		this.fechaIngreso = fechaIngreso;
		this.esGuardia = true;
	}

	public Fecha consultarFechaIngreso() {
		return this.fechaIngreso;
	}

	public Medico consultarMedico() {
		return this.medico;
	}

	public Especialidad consultarEspecialidadDeAtencion() {
		return this.especialidad;
	}

	public boolean atencionGuardia() {
		return this.esGuardia;
	}

	public void asentarPago() {
		pagoAtencion = true;
	}

	public boolean devolverEstadoPago() {
		return pagoAtencion;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[fechaIngreso: ").append(fechaIngreso);
		sb.append(", especialidad: ").append(especialidad);
		sb.append(", medico: ").append(medico);
		sb.append(", esGuardia: ").append(esGuardia);
		sb.append(", pagoAtencion: ").append(pagoAtencion).append("]");
		return sb.toString();
	}
}
